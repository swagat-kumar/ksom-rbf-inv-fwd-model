/* ---------------------------------------------------------
 * Learning Inverse kinematics using RBFN
 *
 * The clusters are created in Input space using KSOM clustering
 *
 * For each input cluster, multiple joint-angle clusters are created
 * using C-Mean cluster (sub-clustering in joint-angle space)
 *
 * Finally these joint-angle clusters are taken as the centers for RBF
 * network.
 *
 * Now, only weights are updated to learn the forward kinematics
 *
 * Finally, the network is inverted to obtain inverse kinematic
 * solution.
 *
 * Expectations:
 *
 * 1. Lesser number of training patterns needed for training
 *
 * 2. More accuracy in the level of millimeter.
 *
 * 3. No retraining would be necessary during inversion.
 *
 * Date: 10 August 2008, Sunday
 *
 * --------------------------------------------------------- */

#include <iostream>
#include <cstdlib>
#include <fstream>
#include <ksom_vmc_sc.h>
#include <powercube_7dof_cam.h>
#include <joint_position.h>
#include <nnet.h>
#include <mymatrix.h>

#define NC 3

using namespace std;
using namespace nnet;

//#define CLUSTER  //train KSOM-SC hint generator
#define TRAIN_RBF  // Train RBF network to learn FK model
//#define RBF_TEST_RP  // TEST the RBF model
//#define INVERT_RBF  // Invert the RBF model using inv-fwd model
//#define INVERT_RP    // inv-fwd model with KSOM-SC hint generator

int main()
{
  //KSOM Network 
  int N[6] = {7, 7, 7, 3, 6, 100};
  ksom3 A(N);
  A.netinfo();

  int node_cnt = 7 * 7 * 7;
  int nsc[344];

  //------------------------

#ifdef CLUSTER

  A.parameter_init(1, -1);

  double eta1_init = 0.9, eta1_fin = 0.9;
  double eta2_init = 0.9, eta2_fin = 0.9;
  double sig_init = 0.9, sig_fin = 0.1; 

  int NMAX = 70000;

  ofstream f1("input.txt");

  cout << "Clustering starts ... wait! " << endl;

  //Loop starts here ...
  double avge = 0.0;
  for(int n = 1; n <= NMAX; n++)
  {
    double Uc[NC], th[NL], Ucn[NC], thn[NL];
    double eta[3];

    //generate a random target  
    generate_data(Uc, th);
    cart_posn_normalize(Uc, Ucn);
    angle_normalize(th, thn);

    for(int i = 0; i < NC; i++)
      f1 << Ucn[i] << "\t";
    for(int i = 0; i < NL; i++)
      f1 << thn[i] << "\t";
    f1 << endl;

    //Learning rate
    eta[0] = eta1_init * pow( (eta1_fin / eta1_init), 
        (n / (double)NMAX) );

    eta[1] = 0.7; // for sub-clustering

    // Sigma necessary for weight update
    eta[2] = sig_init * pow( (sig_fin / sig_init), 
        (n / (double)NMAX) );

    A.create_clusters(eta, Ucn, thn, 1.0);

  }//n-loop
  f1.close();

  A.save_parameters();

  cout << "clustering done ... parameter saved!" << endl;
#endif
  
  //--------------------------


  ifstream fd("ksom_theta.txt");

  int cen_cnt = 0;
  for(int i = 0; i < node_cnt; i++)
  {
    fd >> nsc[i];
    cen_cnt += nsc[i];
  }

  cout << "Total number of centers = " << cen_cnt << endl;

  double **CM = mymatrix(cen_cnt, NL);

  for(int i = 0; i < cen_cnt; i++)
    for(int j = 0; j < NL; j++)
      fd >> CM[i][j];


  fd.close();


  //create a RBF network

  int N2[3] = {NL, cen_cnt, NC};
  double af[2] = {5, 0.3}; //gaussian activation
  double param[3] = {1, -1, 0}; //input range = -1 to 1

  rbfn B(N2, af, param);

  cout << B << endl;

  //-------------------------


#ifdef TRAIN_RBF

  //Read centers 
  B.load_centers(CM);

  double *cv = myvector(cen_cnt*NL);
  double *wt = myvector(NC*cen_cnt);

  cout << "\nTraining The RBF Network ...\n";
  cout << "Only weights are updated. \n" << endl;

  double q[NL], qn[NL], x[NC], xn[NC];
  double xt[NC], xtn[NC];

  int NMAX = 500000;

  ofstream f1("error.txt");
  ofstream f2("tdata.txt");
  ofstream f3("check_cen.txt");

  double savg = 0.0;
  for(int n = 1; n <= NMAX; n++)
  {
    generate_data(xt, q);

    angle_normalize(q, qn);
    cart_posn_normalize(xt, xtn);

    for(int i = 0; i < NL; i++)
      f2 << qn[i] << "\t";
    for(int i = 0; i < NC; i++)
      f2 << xtn[i] << "\t";
    f2 << endl;

    B.network_output(qn, xn);

    //update only weights
    B.backprop(xtn, 0.0, 0.1);

    B.get_parameters(wt, cv);


    f3 << cv[4] << "\t" << cv[10] << "\t" << cv[200] << "\t" << wt[0] << "\t" << wt[100] << endl;


    cart_posn_denormalize(xn, x);

    double s1 = 0.0;
    for(int i = 0; i < NC; i++)
      s1 += pow((xt[i] - x[i]), 2.0);
    s1 = sqrt(s1);
    savg += s1;

    f1 << savg/n << endl;
  }

  f1.close();
  f2.close();
  f3.close();

  B.save_parameters("rbfn_bpar_bp.txt");

  cout << " Training over ... parameters saved ...\n" << endl;

  delete [] CM;

#endif

  //----------------------------------
  
#ifdef RBF_TEST_RP

  cout << "\n\nTesting RBF on random points ... " << endl;

  B.load_parameters("rbfn_bpar_bp.txt");

  ofstream ft("test.txt");

  double xt[NC], xtn[NC], q[NL], qn[NL], x[NC], xn[NC];

  int TMAX = 10000;

  double savg2 = 0.0;
  for(int n = 1; n <= TMAX; n++)
  {
    generate_data(xt, q);

    angle_normalize(q, qn);
    cart_posn_normalize(xt, xtn);

    for(int i = 0; i < NL; i++)
      ft << qn[i] << "\t";
    for(int i = 0; i < NC; i++)
      ft << xtn[i] << "\t";
    ft << endl;

    B.network_output(qn, xn);
    cart_posn_denormalize(xn, x);


    double s1 = 0.0;
    for(int i = 0; i < NC; i++)
      s1 += pow((xt[i] - x[i]), 2.0);
    s1 = sqrt(s1);
    savg2 += s1;
  }
  ft.close();

  cout << "Avg error = " << savg2/TMAX << " m" << endl;
#endif

  //-----------------------------------------

#ifdef INVERT_RBF

  cout << "\n\nNetwork inversion ... " << endl;

  B.load_parameters("rbfn_bpar_bp.txt");

  double **jp = matrix(5,3);

  double qt[NL], qtn[NL];
  double xr[NC], xrn[NC];
  double xt[NC], xtn[NC];

  //Generate a random point in the workspace
  generate_data(xt, qt);

  //normalize the target position
  cart_posn_normalize(xt, xtn);
  angle_normalize(qt, qtn);

  double qc[NL], xc[NC], xcn[NC], qcn[NL];

  //specify the current robot configuration
  qc[0] = 0.0; qc[1] = DEG2RAD(80);
  qc[2] = 0.0; qc[3] = 0.0; qc[4] = 0.0; qc[5] = 0.0;

  qc[0] = qc[0] - pi/2.0;

  //normalize the current angle configuration
  angle_normalize(qc, qcn);

  //current robot end-effector position
  powercube_7dof_FK(qc, xr);

  //current end-effector position obtained from NN
  B.network_output(qcn, xcn); 
  cart_posn_denormalize(xcn, xc);

  ofstream ft("angle.txt");
  ofstream fr("rconfig.txt");
  ofstream fp("efposn.txt");
  ofstream fe1("error.txt");

  for(int i = 0; i < NC; i++)
    fp << xt[i] << "\t";
  for(int i = 0; i < NC; i++)
    fp << xt[i] << "\t";
  fp << endl << endl << endl;



  double se1, se2;
  int step = 0;
  do
  {
    for(int i = 0; i < NC; i++)
      fp << xc[i] << "\t";
    for(int i = 0; i < NC; i++)
      fp << xr[i] << "\t";
    fp << endl;

    jp = joint_posn(qc);

    for(int i = 0; i < 5; i++)
    {
      for(int j = 0; j < 3; j++)
        fr << jp[i][j] << "\t";
      fr << endl;
    }
    fr << endl << endl; 

    //update input using gradient_descent
    // qcn is the updated input

    B.input_jacobian();
    B.update_input_gd(0.05, xtn, qcn);

    //new end-effector position
    B.network_output(qcn, xcn);
    cart_posn_denormalize(xcn, xc);


    angle_denormalize(qcn, qc);
    powercube_7dof_FK(qc, xr);
    cart_posn_normalize(xr, xrn);

    B.backprop(xrn, 0.0, 0.7);


    se1 = 0.0;
    for(int i = 0;i < NC; i++)
      se1 += pow((xc[i] - xt[i]), 2.0);
    se1 = sqrt(se1);

    fe1 << se1 << endl;

    step = step + 1;

    if(step > 400)
      break;

  }while(se1 > 0.01);

  cout << "No. of steps = " << step << endl;


  ft.close();
  fp.close();
  fr.close();
  fe1.close();

  delete [] jp;

#endif //INVERT module ends here

  //-----------------------------------

#ifdef INVERT_RP

  B.load_parameters("rbfn_bpar_bp.txt");

  int TMAX = 10000;

  double qt[NL], qtn[NL];
  double xr[NC], xrn[NC];
  double xt[NC], xtn[NC];
  double qc[NL], xc[NC], xcn[NC], qcn[NL];

  //specify the current robot configuration
  qc[0] = 0.0; qc[1] = DEG2RAD(80);
  qc[2] = 0.0; qc[3] = 0.0; qc[4] = 0.0; qc[5] = 0.0;
  qc[0] = qc[0] - pi/2.0;

  ofstream f1("steps.txt");

  double avg_step = 0.0;
  int rcnt = 0;
  for(int n = 0; n < TMAX; n++)
  {

    //Generate a random point in the workspace
    generate_data(xt, qt);

    //normalize the target position
    cart_posn_normalize(xt, xtn);
    angle_normalize(qt, qtn);


    //normalize the current angle configuration
    angle_normalize(qc, qcn);

    //current robot end-effector position
    powercube_7dof_FK(qc, xr);

    //current end-effector position obtained from NN
    A.network_output(qcn, xcn); 
    cart_posn_denormalize(xcn, xc);

    double se1;
    int step = 0;
    int reject = 0;
    do
    {

      //update input using gradient_descent
      // qcn is the updated input

      A.input_jacobian();
      A.update_input_gd(0.05, xtn, qcn);

      //new end-effector position
      A.network_output(qcn, xcn);
      cart_posn_denormalize(xcn, xc);

      angle_denormalize(qcn, qc);
      powercube_7dof_FK(qc, xr);
      cart_posn_normalize(xr, xrn);

      //A.backprop(xrn, 0.1, 0.7);

      se1 = 0.0;
      for(int i = 0;i < NC; i++)
        se1 += pow((xc[i] - xt[i]), 2.0);
      se1 = sqrt(se1);

      step = step + 1;

      if(step > 400)
      {
        reject = 1;
        break;
      }

    }while(se1 > 0.01);

    if(reject != 1)
    {
      avg_step += step;
      rcnt += 1;
    }
  } //n-loop

  cout << "Avg no. of steps = " << avg_step/(TMAX-rcnt) << endl;

#endif
  return 0;
}


