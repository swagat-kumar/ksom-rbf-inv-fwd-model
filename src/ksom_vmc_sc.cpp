/* KSOM Class Definition for VMC
 *
 *
 * Date: 10 August 2008 Saturday
 * ------------------------------------------ */

#include<fstream>
#include<cmath>
#include <cstdlib>
#include <ksom_vmc_sc.h>

//=============================================================
// theta: sub-cluster array
// nc : sub-cluster count : it is incremented when a new cluster is
// formed
//
// x : input theta vector of dimension DIM x 1
//
//
// ncmax: maximum sub-cluster count
//
// threshold: used for deciding whether to create new cluster
// ---------------------------------------------------------------
void create_subclusters2( double **theta, int *nc, const double *x, int ncmax, double threshold, double eta, int DIM)
{
  double sigma = 0.0001;
  // choose eta = 0.7;

  int count = *nc;
  int winner;

  if(count == 0)
  {
    count = count + 1;

    for(int i = 0; i < DIM; i++)
      theta[count-1][i] = x[i];

  }
  else
  {
    //Find a winner neuron
    double min = 5000;
    for(int i = 0; i < count; i++)
    {
      double sw = 0.0;
      for(int j = 0; j < DIM; j++)
        sw += pow((x[j] - theta[i][j]), 2.0);
      sw = sqrt(sw);

      if(sw < min)
      {
        min = sw;
        winner = i;
      }
    }

    // if minimum distance < threshold
    // this data point belongs to the 
    // winner node winner and hence update
    // the current nodes to represent this data
    // as well. Otherwise create a new node

    if(min < threshold)
    {
      //update centers
      for(int i = 0; i < count; i++)
      {
        //compute lattice distance of each neuron
        //from winner neuron

        double d = pow( (winner-i), 2.0);

        //compute neighborhood function
        double h = exp( -d / (2 * sigma * sigma) );


        //update neuron weights
        for (int l = 0; l < DIM; l++)     
          theta[i][l] += eta * h * (x[l] - theta[i][l]); 

      }
    }
    else // create new clusters 
    {
      if(count < ncmax) 
      {
        count = count + 1;

        for(int i = 0; i < DIM; i++)
          theta[count-1][i] = x[i];
      }
    }

  }// when count > 0

  //Return the number of clusters
  *nc = count;
}//EOF

//=========================================
ksom3::ksom3(int Nip[6])
{
  // Number of nodes in each direction
  Nx = Nip[0];
  Ny = Nip[1];
  Nz = Nip[2];
  NI = Nip[3];  // size of w vector
  NL = Nip[4];  // size of theta vector
  NCMAX = Nip[5]; // maximum no. of theta sub-clusters allowed

  maxNodeCount = Nx * Ny * Nz;


  try{
    W = new double *[maxNodeCount];
    TH = new double **[maxNodeCount];
    A = new double **[maxNodeCount];
    A0 = new double *[NL];
    NC = new int [maxNodeCount];

    for(int i = 0; i < NL; i++)
    {
      A0[i] = new double [NI];
      for(int j = 0; j < NI; j++)
        A0[i][j] = 0.0;
    }

    idx = new int **[Nx];
    int cnt = 0;
    for(int i = 0; i < Nx; i++)
    {
      idx[i] = new int *[Ny];
      for(int j = 0; j < Ny; j++)
      {
        idx[i][j] = new int [Nz];
        for(int k = 0; k < Nz; k++)
        {
          idx[i][j][k] = cnt;
          cnt = cnt + 1;
        }
      }
    }

    for(int i = 0; i < maxNodeCount; i++)
    {
      NC[i] = 0;
      W[i] = new double[NI];
      TH[i] = new double *[NCMAX];
      A[i] = new double *[NL];

      for(int j = 0; j < NL; j++)
        A[i][j] = new double [NI];

      for(int j = 0; j < NCMAX; j++)
        TH[i][j] = new double [NL];
    }

  }catch(bad_alloc){
    cout << "Memory allocation failure in ksom class" << endl;
    exit(-1);
  }

  int i = 0;
  for(int m = 0; m < Nx; m++)
    for(int n = 0; n < Ny; n++)
      for(int p = 0; p < Nz; p++)
      {
        for(int j = 0; j < NI; j++)
          W[i][j] = -1.0 + 2.0 * rand()/(double)RAND_MAX;

        for(int j = 0; j < NL; j++)
          for(int k = 0; k < NI; k++)
            A[i][j][k] = -0.1 + 0.2 * rand()/(double)RAND_MAX; 

        for(int j = 0; j < NCMAX; j++)
          for(int k = 0; k < NL; k++)
            TH[i][j][k] = -1.0 + 2.0 * rand()/(double)RAND_MAX;

        i = i + 1;
      }
}//EOF
//==============================
//Copy Contructor
ksom3::ksom3(const ksom3 &a)
{
  // Number of nodes in each direction
  Nx = a.Nx;
  Ny = a.Ny;
  Nz = a.Nz;
  NI = a.NI;  // size of w vector
  NL = a.NL;  // size of output vector
  NCMAX = a.NCMAX; //max. no. of sub-clusters allowed

  maxNodeCount = a.maxNodeCount;

  try
  {
    NC = new int [maxNodeCount];
    W = new double *[maxNodeCount];
    TH = new double **[maxNodeCount];
    A = new double **[maxNodeCount];
    A0 = new double *[NL];

    for(int i = 0; i < NL; i++)
    {
      A0[i] = new double [NI];
      for(int j = 0; j < NI; j++)
        A0[i][j] = a.A0[i][j];
    }

    idx = new int **[Nx];
    int cnt = 0;
    for(int i = 0; i < Nx; i++)
    {
      idx[i] = new int *[Ny];
      for(int j = 0; j < Ny; j++)
      {
        idx[i][j] = new int [Nz];
        for(int k = 0; k < Nz; k++)
        {
          idx[i][j][k] = cnt;
          cnt = cnt + 1;
        }
      }
    }

    for(int i = 0; i < maxNodeCount; i++)
    {
      W[i] = new double[NI];
      TH[i] = new double *[NCMAX];
      A[i] = new double *[NL];

      for(int j = 0; j < NL; j++)
        A[i][j] = new double [NI];

      for(int j = 0; j < NCMAX; j++)
        TH[i][j] = new double [NL];
    }

  }catch(bad_alloc)
  {
    cout << "Memory allocation in ksom3 copy constructor failed" << endl;
    exit(-1);
  }

  for(int i = 0; i < maxNodeCount; i++)
  {
    NC[i] = a.NC[i];
    for(int j = 0; j < NI; j++)
      W[i][j] = a.W[i][j];

    for(int j = 0; j < NL; j++)
      for(int k = 0; k < NI; k++)
        A[i][j][k] = a.A[i][j][k];

    for(int j = 0; j < NCMAX; j++)
      for(int k = 0; k < NL; k++)
        TH[i][j][k] = a.TH[i][j][k];
  }

}//EOF
//================================================
ksom3::~ksom3()
{
  delete [] A;
  delete [] W;
  delete [] TH;
  delete [] idx;
  delete [] A0;
  delete [] NC;
}
//============================================
void ksom3::netinfo()
{
  cout << "-------- *** -----------" << endl;
  cout << "KSOM Lattice size = " << Nx << " x " << Ny << " x " << Nz << endl;
  cout << "NL. of Inputs = " << NI << endl;
  cout << "No. of Outputs = " << NL << endl;
  cout << "Total number of nodes = " << maxNodeCount << endl;
  cout << "Dimension of W = " << maxNodeCount << " x " << NI << endl;
  cout << "Dimension of TH = " << maxNodeCount << " x " << NCMAX << " x " << NL << endl;
  cout << "Dimension of A = " << maxNodeCount << " x " << NL << " x " << NI << endl;
  cout << "-------- *** -----------" << endl;

}//EOF 
//=============================================

void ksom3::save_parameters()
{
  ofstream fw("ksom_weight.txt");
  ofstream ft("ksom_theta.txt");
  ofstream fa("ksom_jacob.txt");

  for(int l = 0; l < Nx; l++)
    for(int m = 0; m < Ny; m++)
      for(int n = 0; n < Nz; n++)
      {
        for(int j = 0; j < NI; j++)
          fw << W[idx[l][m][n]][j] << "\t";
        fw << endl;

        ft << NC[idx[l][m][n]] << endl;

        for(int j = 0; j < NL; j++)
        {
          for(int k = 0; k < NI; k++)
            fa << A[idx[l][m][n]][j][k] << "\t";
          fa << endl;
        }
        fa << endl << endl;;
      }

  ft << endl << endl;

  for(int i = 0; i < maxNodeCount; i++)
  {
    for(int j = 0; j < NC[i]; j++)
    {
      for(int k = 0; k < NL; k++)
        ft << TH[i][j][k] << "\t";
      ft << endl;
    }
    ft << endl << endl;
  }
    
  fw.close();
  ft.close();
  fa.close();
}

//==========================================

void ksom3::read_parameters()
{
  ifstream fw("ksom_weight.txt");
  ifstream ft("ksom_theta.txt");
  ifstream fa("ksom_jacob.txt");

  for(int l = 0; l < Nx; l++)
    for(int m = 0; m < Ny; m++)
      for(int n = 0; n < Nz; n++)
      {
        for(int j = 0; j < NI; j++)
          fw >> W[idx[l][m][n]][j] ;

        ft >> NC[idx[l][m][n]];

        for(int j = 0; j < NL; j++)
          for(int k = 0; k < NI; k++)
            fa >> A[idx[l][m][n]][j][k];
      }

  for(int i = 0; i < maxNodeCount; i++)
  {
    for(int j = 0; j < NC[i]; j++)
      for(int k = 0; k < NL; k++)
        ft >> TH[i][j][k];
  }


  fw.close();
  ft.close();
  fa.close();
}

//==========================================
// Clusters in input (W) space is created as per KSOM algorithm
// More than one output cluster is created for each input cluster.
// --------------------------------------------------------------
void ksom3::create_clusters(const double eta[], const double *xd, const double *thd, double threshold)
{
  int w_idx[3];

  double eta_w = eta[0];
  double eta_t = eta[1];
  double sigma = eta[2];

  // Select a winner in cartesian space
  double Min_w = 50000;
  for(int i = 0; i < Nx; i++)
    for(int j = 0; j < Ny; j++)
      for(int k = 0; k < Nz; k++)
      {
        double sw = 0.0;
        for(int l = 0; l < NI; l++)  
          sw += pow((W[idx[i][j][k]][l] - xd[l]), 2.0);
        double zw = sqrt(sw);

        if(zw < Min_w)
        {
          Min_w = zw;
          w_idx[0] = i;
          w_idx[1] = j;
          w_idx[2] = k;
        }

      }//for each index

  int windex = idx[w_idx[0]][w_idx[1]][w_idx[2]];

  //create subclusters in theta space for each winner
  create_subclusters2(TH[windex], &NC[windex], thd, NCMAX, threshold, eta_t, NL);

  //Update parameters
  for(int i = 0; i < Nx; i++)
    for(int j = 0; j < Ny; j++)
      for(int k = 0; k < Nz; k++)
      {
        //compute lattice distance of each neuron
        //from winner neuron
        double dw = pow((w_idx[0]-i), 2.0) + 
          pow((w_idx[1]-j), 2.0) + pow((w_idx[2]-k), 2.0);

        //compute neighborhood function
        double hw = exp( -dw / (2 * sigma * sigma) );

        //update neuron weights
        for (int l = 0; l < NI; l++) 
          W[idx[i][j][k]][l] += eta_w * hw * (xd[l] - W[idx[i][j][k]][l]); 

      }// Neuron index ... for each neuron    


}//EOF
//==============================================
/*void ksom3::update_jacobian(double dvn[], double dthn[], double eta[])
{
  double *DTH = new double [NL];
  double eta_a = eta[0];
  double sigma = eta[1];

  for(int i = 0; i < NL; i++)
  {
    double s = 0.0;
    for(int j = 0; j < NI; j++)
      s += A0[i][j] * dvn[j];
    DTH[i] = dthn[i] - s;
  }

  double dvnormsq = 0.0;
  for(int i = 0; i < NI; i++)
    dvnormsq += dvn[i] * dvn[i];

  if(dvnormsq < 1e-3)
    dvnormsq = dvnormsq + 0.01;

  for(int i = 0; i < Nx; i++)
    for(int j = 0; j < Ny; j++)
      for(int k = 0; k < Nz; k++)
      {
        double dw = pow((windex[0]-i), 2.0) + 
          pow((windex[1]-j), 2.0) + pow((windex[2]-k), 2.0);
        double hw = exp( -dw / (2 * sigma * sigma) );

        for(int l = 0; l < NL; l++)
          for(int m = 0; m < NI; m++)
            A[idx[i][j][k]][l][m] += eta_a * hw * DTH[l] * dvn[m] / (shw * dvnormsq);
      }

  delete [] DTH;
}//EOF */
//=====================================
/*void ksom3::get_jacob(double ***a = NULL)
{
  if(a == NULL) 
    return;
  else
  {
    for(int i = 0; i < Nx; i++)
      for(int j = 0; j < Ny; j++)
        for(int k = 0; k < Nz; k++)
        {
          for(int l = 0; l < NL; l++)
            for(int m = 0; m < NI; m++)
              a[idx[i][j][k]][l][m] = A[idx[i][j][k]][l][m];
        }
  }
}//EOF */
//=======================================
/*double * ksom3::coarse_movement(const double xd[], double sigma)
{
  double Min_w = 5000;
  for(int i = 0; i < Nx; i++)
    for(int j = 0; j < Ny; j++)
      for(int k = 0; k < Nz; k++)
      {
        double sw = 0.0;
        for(int l = 0; l < NI; l++)  
          sw += pow((W[idx[i][j][k]][l] - xd[l]), 2.0);
        double zw = sqrt(sw);

        if(zw < Min_w)
        {
          Min_w = zw;
          windex[0] = i;
          windex[1] = j;
          windex[2] = k;
        }
      }

  double *st = new double [NL];
  double *th0 = new double [NL];
  double **sA = new double *[NL];

  for(int i = 0; i < NL; i++)
  {
    st[i] = 0;
    sA[i] = new double [NI];
    for(int j = 0; j < NI; j++)
      sA[i][j] = 0.0;
  }

  shw = 0.0;
  for(int i = 0; i < Nx; i++)
    for(int j = 0; j < Ny; j++)
      for(int k = 0; k < Nz; k++)
      {
        double dw = pow((windex[0]-i), 2.0) + 
          pow((windex[1]-j), 2.0) + pow((windex[2]-k), 2.0);

        double hw = exp( -dw / (2 * sigma * sigma) );

        for(int l = 0; l < NL; l++)
        {
          double s1 = 0.0;
          for(int m = 0; m < NI; m++)
          {
            s1 += A[idx[i][j][k]][l][m] * (xd[m] - W[idx[i][j][k]][m]);
            sA[l][m] += hw * A[idx[i][j][k]][l][m];
          }
          st[l] += hw * (TH[idx[i][j][k]][l] + s1);
        }
        shw += hw;
      }//for each lattice index

  for(int i = 0; i < NL; i++)
  {
    th0[i] = st[i] / shw;
    //if(th0[i] > 1.0)
    //  th0[i] = 1.0;
    //else if(th0[i] < -1.0)
    //  th0[i] = -1.0;

    for(int j = 0; j < NI; j++)
      A0[i][j] = sA[i][j] / shw;
  }

  delete [] st;
  delete [] sA;

  return (th0); //this is a memory in heap and hence address can be returned
}//EOF */
//================================
/*double * ksom3::fine_movement(const double xt[], const double V0[], const double theta0[])
{
  double *theta1 = new double [NL];
  for(int i = 0; i < NL; i++)
  {
    double s1 = 0.0;
    for(int j = 0; j < NI; j++)
      s1 += A0[i][j] * (xt[j] - V0[j]);
    theta1[i] = theta0[i] + s1;

    //if(theta1[i] > 1.0)
    //  theta1[i] = 1.0;
    //else if(theta1[i] < -1.0)
    //  theta1[i] = - 1.0; 
  }

  return(theta1);
}//EOF */
//====================================
void ksom3::parameter_init(double max, double min)
{
  for(int i = 0; i < Nx; i++)
    for(int j = 0; j < Ny; j++)
      for(int k = 0; k < Nz; k++)
      {
        for(int l = 0; l < NI; l++)
          W[idx[i][j][k]][l] = min + (max - min) * rand()/(double)RAND_MAX;

        for(int l = 0; l < NL; l++)
          for(int m = 0; m < NI; m++)
            A[idx[i][j][k]][l][m] =  min + (max - min) * rand()/(double)RAND_MAX; 

        for(int l = 0; l < NCMAX; l++)
          for(int m = 0; m < NL; m++)
            TH[idx[i][j][k]][l][m] = min + (max - min) * rand()/(double)RAND_MAX;
      }
}//EOF
//=================================




