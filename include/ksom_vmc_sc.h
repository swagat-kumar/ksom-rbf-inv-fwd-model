/* KSOM Class Definition for VMC + Sub-clustering
 *
 * Date: 10 August 2008 Sunday
 * --------------------------------- */  

#ifndef _KSOM_VMC_SC
#define _KSOM_VMC_SC

#include<iostream>
#include<new>

using namespace std;

class ksom3
{
  private:
    int ***idx, NI, NL, NCMAX, maxNodeCount;
    int Nx, Ny, Nz, windex[3], *NC;
    double **W, ***A, ***TH; 
    double **A0, shw;

  public:
    ksom3(int N[]);
    ksom3(const ksom3 &a);
    ~ksom3();
    void netinfo();
    void save_parameters();
    void read_parameters();
    void create_clusters(const double eta[], const double *xd,const double *thd, double threshold);
//    double* coarse_movement(const double xd[], double sigma);
//    double* fine_movement(const double xt[], const double V0[], const double *theta0);
//    void update_jacobian(double dvn[], double dthn[], double eta[3]);
//    void get_jacob(double ***a);
    void parameter_init(double max, double min);
};

#endif
//-------------------------------------                

/* --------------------------------------------------
 * Documentation:
 * -----------------------------------------------------
 *
 *  Variables::
 *  -----------
 *
 * NI: Dimension of input space
 *
 * NL: Dimension of output space
 *
 * NCMAX: Maximum number of sub-clusters associated with each input
 * cluster
 *
 * NC : maxNodeCount x 1 : No. of theta sub-clusters associated with
 * each input cluster
 *
 * maxNodeCount : maximum no. of nodes = Nx * Ny * Nz
 *
 * Nx * Ny * Nz : size of KSOM lattice
 *
 * W : maxNodeCount x NI
 *
 * A : maxNodeCount x NL x NI
 *
 * TH : maxNodeCount x NCMAX x NL
 *
 * idx: contains the lattice index of each node
 *
 * A0 : temporary matrix of dimension NL x NI
 *
 *
 * Functions::
 * -----------
 *
 *  1. ksom3(int N[]);
 *
 *  constructor: initializes the KSOM network and allocates memory
 *
 *  Input: 6 dimensional vector = {Nx, Ny, Nz, NI, NL, NCMAX};
 *  Output: None
 *
 *  2. ksom3(const ksom3f &a); 
 *
 *  Copy Constructor
 *
 *  3. ~ksom3();
 *
 *  Destructor: Destroys the object and deallocates memory
 *
 *  4. create_clusters(eta, xd, thd);
 *
 *  Clusters are created in input and output spaces based on pattern
 *  (xd, thd);
 *
 *  More than one output cluster is created for each input cluster.
 *
 *
 *
 *
 *
 * ------------------------------------------------------------- */

