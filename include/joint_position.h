#ifndef _JTPOS_
#define _JTPOS_

#include<cmath>
#include <mymatrix.h>


double** joint_posn(double th[])
{
  double **x;
  x = mymatrix(5,3);

  double **A1, **A2, **A3;
  double **A4, **A5, **A6, **A7;
  double t1, t2, t3, t4, t5, t6, t7;

  t1 = th[0];
  t2 = th[1];
  t3 = th[2];
  t4 = th[3];
  t5 = th[4];
  t6 = th[5];
  t7 = th[6];

  // D-H Parameters (m)
  double d1 = 0.390, d3 = 0.370; 
  double d5 = 0.310, d7 = 0.2656;

  double a1[16] = {cos(t1), 0, sin(t1), 0,
    sin(t1), 0, -cos(t1), 0, 
    0, 1, 0, d1,
    0, 0, 0, 1};

  double a2[16] = {cos(t2), 0, -sin(t2), 0,
    sin(t2), 0, cos(t2), 0,
    0, -1, 0, 0,
    0, 0, 0, 1};

  double a3[16] = {cos(t3), 0, sin(t3), 0,
    sin(t3), 0, -cos(t3), 0,
    0, 1, 0, d3,
    0, 0, 0, 1};

  double a4[16] = {cos(t4), 0, -sin(t4), 0,
    sin(t4), 0, cos(t4), 0,
    0, -1, 0, 0,
    0, 0, 0, 1};

  double a5[16] = {cos(t5), 0, sin(t5), 0,
    sin(t5), 0, -cos(t5), 0,
    0, 1, 0, d5,
    0, 0, 0, 1};

  double a6[16] = {cos(t6), 0, -sin(t6), 0,
    sin(t6), 0, cos(t6), 0,
    0, -1, 0, 0,
    0, 0, 0, 1};

  double a7[16] = {cos(t7), -sin(t7), 0, 0,
    sin(t7), cos(t7), 0, 0,
    0, 0, 1, d7,
    0, 0, 0, 1};

  //Transformation matrices
  A1 = mymatrix(a1, 4, 4);
  A2 = mymatrix(a2, 4, 4);
  A3 = mymatrix(a3, 4, 4);
  A4 = mymatrix(a4, 4, 4);
  A5 = mymatrix(a5, 4, 4);
  A6 = mymatrix(a6, 4, 4);
  A7 = mymatrix(a7, 4, 4);

  double **A02, **A04, **A06, **A07;
  double **A12;

  A02 = mymatrix(4, 4);
  A04 = mymatrix(4, 4);
  A06 = mymatrix(4, 4);
  A07 = mymatrix(4, 4);
  A12 = mymatrix(4, 4);

  A02 = mult(A1, A2, 4);
  A04 = mult(mult(A02, A3, 4), A4, 4);
  A06 = mult(mult(A04, A5, 4), A6, 4);
  A07 = mult(A06, A7, 4);

  //Base of the robot
  x[0][0] = 0; x[0][1] = 0; x[0][2] = 0;

  for(int i = 0; i < 3; i++)
  {
    x[1][i] = A02[i][3];
    x[2][i] = A04[i][3];
    x[3][i] = A06[i][3];
    x[4][i] = A07[i][3];
  }

  return x;
}
//=========================
//
#endif


