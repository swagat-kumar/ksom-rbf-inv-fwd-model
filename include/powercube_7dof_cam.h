#ifndef _VMC_
#define _VMC_

#include <cmath>
#include <cstdlib>

#define pi 3.141592654

const int NL = 6;			// No. of Manipulator Links
const int NI = 4;			// No. of Manipulator Links
const int NC = 3;


#define DEG2RAD(x) pi*x/180.0
#define RAD2DEG(x) 180.0 * x / pi

using namespace std;

// Get these values from 'train_nw.cpp'.
const double theta_max[7] = {1.3, 1.7, 2.8, 2.15, 1.7, 2.15, 6.5};
const double theta_min[7] = {-4.4, -1.7, -2.8, -0.9, -1.7, -2.15, -6.5};

// Cartesian range
const double c_max[3] = {0.4, 0.8, 0.4};
const double c_min[3] = {-0.4, 0.3, -0.1}; 

//Range in camera space
const double x_max[4] = {280, 160, 210, 170};
const double x_min[4] = {70, 0, 10, 0}; 


/* ------------------------------------------------*/
// NOrmalization routine ....
void cam_posn_normalize(double x[], double xn[])
{
  int i;
  for(i = 0; i < NI; i++)
    xn[i] = 2.0 *((x[i] - x_min[i])/(x_max[i] - x_min[i]) - 0.5);
}
//--------------------------------------------
void cart_posn_normalize(double x[], double xn[])
{
  int i;
  for(i = 0; i < 3; i++)
    xn[i] = 2.0 *((x[i] - c_min[i])/(c_max[i] - c_min[i]) - 0.5);
}
//--------------------------------------------
void cam_posn_denormalize(double xn[], double x[])
{
  int i;
  for(i = 0; i < NI; i++)
    x[i] = x_min[i] + (xn[i]/2.0 + 0.5) * (x_max[i] - x_min[i]);
}
//-------------------------------------------
void cart_posn_denormalize(double xn[], double x[])
{
  int i;
  for(i = 0; i < 3; i++)
    x[i] = c_min[i] + (xn[i]/2.0 + 0.5) * (c_max[i] - c_min[i]);
}
//----------------------------------------
void angle_normalize(double theta[], double th_n[])
{
  int i;
  for(i = 0; i < NL; i++)
    th_n[i] = 2.0 * ((theta[i] - theta_min[i])/
        (theta_max[i] - theta_min[i]) - 0.5);
}
/* ---------------------------------------------- */
void angle_denormalize( double th_n[], double th[])
{
  int i;
  for(i = 0; i < NL; i++)
    th[i] = theta_min[i] + (th_n[i]/2.0 + 0.5) * (theta_max[i] - theta_min[i]);
}
//=============================================================    
// Clockwise theta is positive
inline void powercube_7dof_FK(double Th[], double V[])
{
  double t1,t2,t3,t4,t5,t6;
  const double d3=0.370, d1= 0.390, d5= 0.310 , d7= 0.2656; // in m

  t1=Th[0];
  t2=Th[1];
  t3=Th[2];
  t4=Th[3];
  t5=Th[4];
  t6=Th[5];

  V[0]= (-(((cos(t1)*cos(t2)*cos(t3) - sin(t1)*sin(t3))*cos(t4) -
          cos(t1)*sin(t2)*sin(t4))*cos(t5) + (-cos(t1)*cos(t2)*sin(t3)
          - sin(t1)*cos(t3))*sin(t5))*sin(t6) +
      (-(cos(t1)*cos(t2)*cos(t3) - sin(t1)*sin(t3))*sin(t4) -
       cos(t1)*sin(t2)*cos(t4))*cos(t6))*d7 +
    (-(cos(t1)*cos(t2)*cos(t3) -sin(t1)*sin(t3))*sin(t4) -
     cos(t1)*sin(t2)*cos(t4))*d5 - cos(t1)*sin(t2)*d3;  


  V[1] = (-(((sin(t1)*cos(t2)*cos(t3) + cos(t1)*sin(t3))*cos(t4) -
          sin(t1)*sin(t2)*sin(t4))*cos(t5) + (-sin(t1)*cos(t2)*sin(t3) +
          cos(t1)*cos(t3))*sin(t5))*sin(t6) + (-(sin(t1)*cos(t2)*cos(t3)
          + cos(t1)*sin(t3))*sin(t4) -
        sin(t1)*sin(t2)*cos(t4))*cos(t6))*d7 +
    (-(sin(t1)*cos(t2)*cos(t3) + cos(t1)*sin(t3))*sin(t4) -
     sin(t1)*sin(t2)*cos(t4))*d5 - sin(t1)*sin(t2)*d3;


  V[2]=(-((sin(t2)*cos(t3)*cos(t4) + cos(t2)*sin(t4))*cos(t5) -
        sin(t2)*sin(t3)*sin(t5))*sin(t6) + (-sin(t2)*cos(t3)*sin(t4) +
        cos(t2)*cos(t4))*cos(t6))*d7 + (-sin(t2)*cos(t3)*sin(t4) +
      cos(t2)*cos(t4))*d5 + cos(t2)*d3+d1;

}
//===================================================================================

void generate_data(double Uc[], double Th[])
{
  //Generate a data point
  do
  {
    for(int i = 0; i < NL; i++)
      Th[i] = theta_min[i] + (theta_max[i] - theta_min[i]) * 
        (rand()/(double)RAND_MAX);

    //So that Ut is in the workspace of robot
    powercube_7dof_FK(Th,Uc); 

    if(Uc[0] <= c_max[0] && Uc[0] > c_min[0])
      if(Uc[1] <= c_max[1] && Uc[1] > c_min[1])
        if(Uc[2] <= c_max[2] && Uc[2] > c_min[2])
          break;

  }while(1);  
}

//=========================================================
void pc_jacobian(double *Theta, double **Jacob)
{
  double t1,t2,t3,t4,t5,t6;
  const double d3 = 0.370, d5 = 0.310 , d7 = 0.2656; // in m
  //const double d1 = 0.390; //not necessary

  t1 = Theta[0];
  t2 = Theta[1];
  t3 = Theta[2];
  t4 = Theta[3];
  t5 = Theta[4];
  t6 = Theta[5];


  Jacob[0][0] =(-(((-sin(t1)*cos(t2)*cos(t3)-cos(t1)*sin(t3))*cos(t4)+
          sin(t1)*sin(t2)*sin(t4))*cos(t5)+
        (sin(t1)*cos(t2)*sin(t3)-cos(t1)*cos(t3))*sin(t5))*sin(t6)
      +(-(-sin(t1)*cos(t2)*cos(t3)-cos(t1)*sin(t3))*sin(t4)+
        sin(t1)*sin(t2)*cos(t4))*cos(t6))*d7
    +(-(-sin(t1)*cos(t2)*cos(t3)-cos(t1)*sin(t3))*sin(t4)
        +sin(t1)*sin(t2)*cos(t4))*d5+sin(t1)*sin(t2)*d3;



  Jacob[0][1] = cos(t1)*(d7*sin(t6)*cos(t5)*sin(t2)*cos(t3)*cos(t4)
      +d7*sin(t6)*cos(t5)*cos(t2)*sin(t4)
      -d7*sin(t6)*sin(t2)*sin(t3)*sin(t5)
      +d7*cos(t6)*sin(t2)*cos(t3)*sin(t4) -d7*cos(t6)*cos(t2)*cos(t4)
      +d5*sin(t2)*cos(t3)*sin(t4) -d5*cos(t2)*cos(t4)-cos(t2)*d3);


  Jacob[0][2] =(-((-cos(t1)*cos(t2)*sin(t3)
          -sin(t1)*cos(t3))*cos(t4)*cos(t5)+
        (-cos(t1)*cos(t2)*cos(t3)+sin(t1)*sin(t3))*sin(t5))*sin(t6)-
      (-cos(t1)*cos(t2)*sin(t3)-sin(t1)*cos(t3))*sin(t4)*cos(t6))*d7-
    (-cos(t1)*cos(t2)*sin(t3)-sin(t1)*cos(t3))*sin(t4)*d5;


  Jacob[0][3] =(-(-(cos(t1)*cos(t2)*cos(t3)-sin(t1)*sin(t3))*sin(t4)
        -cos(t1)*sin(t2)*cos(t4))*cos(t5)*sin(t6)
      +(-(cos(t1)*cos(t2)*cos(t3)-sin(t1)*sin(t3))*cos(t4)+
        cos(t1)*sin(t2)*sin(t4))*cos(t6))*d7
    +(-(cos(t1)*cos(t2)*cos(t3)-sin(t1)*sin(t3))*cos(t4)+
        cos(t1)*sin(t2)*sin(t4))*d5;


  Jacob[0][4] =-(-((cos(t1)*cos(t2)*cos(t3)- sin(t1)*sin(t3))*cos(t4)
        -cos(t1)*sin(t2)*sin(t4))*sin(t5)
      +(-cos(t1)*cos(t2)*sin(t3)-sin(t1)*cos(t3))*cos(t5))*sin(t6)*d7;


  Jacob[0][5] =(-(((cos(t1)*cos(t2)*cos(t3)
            -sin(t1)*sin(t3))*cos(t4)-cos(t1)*sin(t2)*sin(t4))*cos(t5)
        +(-cos(t1)*cos(t2)*sin(t3)-sin(t1)*cos(t3))*sin(t5))*cos(t6)
      -(-(cos(t1)*cos(t2)*cos(t3)-sin(t1)*sin(t3))*sin(t4)
        -cos(t1)*sin(t2)*cos(t4))*sin(t6))*d7;


  Jacob[1][0] = (-(((cos(t1)*cos(t2)*cos(t3)-
            sin(t1)*sin(t3))*cos(t4)-cos(t1)*sin(t2)*sin(t4))*cos(t5)
        +(-cos(t1)*cos(t2)*sin(t3)-sin(t1)*cos(t3))*sin(t5))*sin(t6)
      +(-(cos(t1)*cos(t2)*cos(t3)-sin(t1)*sin(t3))*sin(t4)
        -cos(t1)*sin(t2)*cos(t4))*cos(t6))*d7
    +(-(cos(t1)*cos(t2)*cos(t3)-sin(t1)*sin(t3))*sin(t4)
        -cos(t1)*sin(t2)*cos(t4))*d5 -cos(t1)*sin(t2)*d3;

  Jacob[1][1] = -sin(t1)*(-d7*sin(t6)*cos(t5)*sin(t2)*cos(t3)*cos(t4)
      -d7*sin(t6)*cos(t5)*cos(t2)*sin(t4)
      +d7*sin(t6)*sin(t2)*sin(t3)*sin(t5)-d7*cos(t6)*sin(t2)*cos(t3)*sin(t4)
      +d7*cos(t6)*cos(t2)*cos(t4)- d5*sin(t2)*cos(t3)*sin(t4)
      +d5*cos(t2)*cos(t4)+cos(t2)*d3);


  Jacob[1][2] = (-((-sin(t1)*cos(t2)*sin(t3)
          +cos(t1)*cos(t3))*cos(t4)*cos(t5)+ (-sin(t1)*cos(t2)*cos(t3)
          -cos(t1)*sin(t3))*sin(t5))*sin(t6)-
      (-sin(t1)*cos(t2)*sin(t3) +cos(t1)*cos(t3))*sin(t4)*cos(t6))*d7
    -(-sin(t1)*cos(t2)*sin(t3)+cos(t1)*cos(t3))*sin(t4)*d5;



  Jacob[1][3] = (-(-(sin(t1)*cos(t2)*cos(t3) +cos(t1)*sin(t3))*sin(t4)
        -sin(t1)*sin(t2)*cos(t4))*cos(t5)*sin(t6)
      +(-(sin(t1)*cos(t2)*cos(t3) +cos(t1)*sin(t3))*cos(t4)
        +sin(t1)*sin(t2)*sin(t4))*cos(t6))*d7
    +(-(sin(t1)*cos(t2)*cos(t3) +cos(t1)*sin(t3))*cos(t4)
        +sin(t1)*sin(t2)*sin(t4))*d5;

  Jacob[1][4] = -(-((sin(t1)*cos(t2)*cos(t3) +cos(t1)*sin(t3))*cos(t4)
        -sin(t1)*sin(t2)*sin(t4))*sin(t5) +(-sin(t1)*cos(t2)*sin(t3)
        +cos(t1)*cos(t3))*cos(t5))*sin(t6)*d7;


  Jacob[1][5] = (-(((sin(t1)*cos(t2)*cos(t3) +cos(t1)*sin(t3))*cos(t4)
          -sin(t1)*sin(t2)*sin(t4))*cos(t5) +(-sin(t1)*cos(t2)*sin(t3)
          +cos(t1)*cos(t3))*sin(t5))*cos(t6)
      -(-(sin(t1)*cos(t2)*cos(t3) +cos(t1)*sin(t3))*sin(t4)
        -sin(t1)*sin(t2)*cos(t4))*sin(t6))*d7;

  Jacob[2][0] = 0 ;

  Jacob[2][1] =(-((cos(t2)*cos(t3)*cos(t4) -sin(t2)*sin(t4))*cos(t5)
        -cos(t2)*sin(t3)*sin(t5))*sin(t6) +(-cos(t2)*cos(t3)*sin(t4)
        -sin(t2)*cos(t4))*cos(t6))*d7 +(-cos(t2)*cos(t3)*sin(t4)
      -sin(t2)*cos(t4))*d5 -sin(t2)*d3;

  Jacob[2][2] = sin(t2)*(d7*sin(t6)*sin(t3)*cos(t4)*cos(t5) +
      d7*sin(t6)*cos(t3)*sin(t5) + d7*sin(t3)*sin(t4)*cos(t6) +
      sin(t3)*sin(t4)*d5);

  Jacob[2][3] = (-(-sin(t2)*cos(t3)*sin(t4) +
        cos(t2)*cos(t4))*cos(t5)*sin(t6) + (-sin(t2)*cos(t3)*cos(t4) -
        cos(t2)*sin(t4))*cos(t6))*d7 + (-sin(t2)*cos(t3)*cos(t4) -
      cos(t2)*sin(t4))*d5;


  Jacob[2][4] = -(-(sin(t2)*cos(t3)*cos(t4) + cos(t2)*sin(t4))*sin(t5)
      -sin(t2)*sin(t3)*cos(t5))*sin(t6)*d7;

  Jacob[2][5] = (-((sin(t2)*cos(t3)*cos(t4) +cos(t2)*sin(t4))*cos(t5)
        -sin(t2)*sin(t3)*sin(t5))*cos(t6) -(-sin(t2)*cos(t3)*sin(t4)
        +cos(t2)*cos(t4))*sin(t6))*d7;

}//EOF

//--------------------------------------------------
#endif
