This project implements the Neural Network based inverse forward
scheme for solving the inverse kinematics of a redundant manipulator. 

Please refer to the following paper for details of the algorithm:

"Kinematic control of a redundant manipulator using inverse-forward
adaptive scheme with a KSOM based hint generator". 
Swagat Kumar, Laxmidhar Behera and Martin McGinnity. 
Robotics and Autonomous Systems, Vol. 58, No. 5, May 2010. 


Requirements:

OS: GNU/Linux  (tested on Ubuntu 14.04 LTS)
Compiler: GCC

Dependencies:

Please Download the NNLIB package by the same author using the
following GIT link: 

$ git clone https://gitlab.com/swagat-kumar/nnlib.git

Please compile the package for NNLIB. 

Modify the CMakeList.txt file to point to the header files and
libraries from NNLIB package.

Open the file "/src/ksom_rbf.cpp" in any editor and uncomment the
define tags one at time.

Step 1: #define CLUSTER  (trains the KSOM-SC hint generator module)

Step 2: #define TRAIN_RBF (RBF learns the forward kinematic model)

Step 3: #define RBF_TEST_RP (just for testing if RBF is trained
    properly)

Step 4: #define INVERT_RBF (implements the inverse forward algorithm
    to solve the IK problem)

STEP 5: #define INVERT_RP (implements inv-fwd scheme along with
    KSOM-SC hint generator for a set of random points)









